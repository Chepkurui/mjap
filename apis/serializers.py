from rest_framework import serializers
from apis.models import Hospital, Flyer

class hospitalSerializer(serializers.ModelSerializer):
    class Meta:
       model = Hospital
       # fields ='__all__'
       fields = (
            'id',
            'name',
            'created'
        )

class flyerSerializer(serializers.ModelSerializer):
    class Meta:
       model = Flyer
       fields ='__all__'