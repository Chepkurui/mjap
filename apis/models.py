from django.db import models

# Create your models here.
class Hospital(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100) # Needed
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
            return self.name

    class Meta:
            ordering = ['created']

class Flyer(models.Model):
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=100, blank=True, null=True)
    body = models.CharField(max_length=500, blank=True, null=True)
    image = models.CharField(max_length=100, blank=True, null=True) # Needed
    created = models.DateTimeField(auto_now_add=True)