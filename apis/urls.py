from django.urls import path
from .views import hospitalAPIView, flyerAPIView

urlpatterns = [
    # Hospitals
    path('hospitals', hospitalAPIView.as_view()),
    path('hospitals/<str:pk>', hospitalAPIView.as_view()),

    # Flyers
    path('flyers', flyerAPIView.as_view()),
    path('flyers/<str:pk>', flyerAPIView.as_view()),
]