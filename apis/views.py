import os
import uuid

from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser
from rest_framework.decorators import parser_classes

from apis.models import Hospital, Flyer
from apis.serializers import hospitalSerializer, flyerSerializer

import json

from django.http.response import Http404
from django.shortcuts import render

class hospitalAPIView(APIView):

    # READ a single Todo
    def get_object(self, pk):
        try:
            # return userProfile.objects.get(pk=pk)
            return Hospital.objects.filter(pk=pk)
        except Hospital.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            data = self.get_object(pk)
        else:
            data = Hospital.objects.all()

        serializer = hospitalSerializer(data, many=True)

        return Response(serializer.data)

    def post(self, request, format=None):

        print(request.data)

        data = request.data
        serializer = hospitalSerializer(data=data)

        serializer.is_valid(raise_exception=True)

        serializer.save()

        response = Response()

        response.data = {
            'message': 'Hospital Created Successfully',
            'data': serializer.data
        }

        return response

    def put(self, request, pk=None, format=None):
        todo_to_update = Hospital.objects.get(pk=pk)
        serializer = hospitalSerializer(instance=todo_to_update,data=request.data, partial=True)

        serializer.is_valid(raise_exception=True)

        serializer.save()

        response = Response()

        response.data = {
            'message': 'Hospital Updated Successfully',
            'data': serializer.data
        }

        return response

    def delete(self, request, pk, format=None):
        todo_to_delete =  Hospital.objects.get(pk=pk)

        todo_to_delete.delete()

        return Response({
            'message': 'Hospital Deleted Successfully'
        })

@parser_classes((MultiPartParser, ))
class flyerAPIView(APIView):

    def post(self, request, format=None):
        # thumbnail = request.FILES["image"]
        data = request.data
        # branch = data['branch'] # branch = request.data['branch']
        # folder = data['folder'] # folder = request.data['folder'] 
        # info = json.loads(request.data['info'])
        print(data)
        if request.FILES.get("file", None) is not None:

            #So this would be the logic
            img = request.FILES["file"]
            # img_name = str(uuid.uuid4()) + "-" + os.path.splitext(img.name)[0]
            img_name = str(uuid.uuid4()) + "-" + os.path.splitext(img.name)[0]
            img_extension = os.path.splitext(img.name)[1]
            print('Name of Image')
            print(img.name)

            # This will generate random folder for saving your image using UUID
            # save_path = "media/" + str(uuid.uuid4())
            save_path = "media/flyers" # + folder #+ "/"
            if not os.path.exists(save_path):
                # This will ensure that the path is created properly and will raise exception if the directory already exists
                # os.makedirs(os.path.dirname(save_path), exist_ok=True)
                os.makedirs(save_path)
            
            # Create image save path with title
            img_save_path = "%s/%s%s" % (save_path, img_name, img_extension)
            with open(img_save_path, "wb+") as f:
                for chunk in img.chunks():
                    f.write(chunk)
            # data = {"success": True, "image": img.name}
            serializer = flyerSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save(image=img_name + img_extension)
            # response = Response()
            feedback = {
                'message': 'Flyer Created Successfully',
                'data': serializer.data,
            }
        # ...
        else:
            serializer = flyerSerializer(data=data)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            # response = Response()
            feedback = {
                'message': 'Flyer Created Successfully',
                'data': serializer.data,
            }
        return Response(feedback)

    # READ a single Todo
    def get_object(self, pk):
        try:
            # return userProfile.objects.get(pk=pk)
            return Flyer.objects.filter(pk=pk)
        except Flyer.DoesNotExist:
            raise Http404

    def get(self, request, pk=None, format=None):
        if pk:
            data = self.get_object(pk)
        else:
            data = Flyer.objects.all()

        serializer = flyerSerializer(data, many=True)

        return Response(serializer.data)

    def put(self, request, pk=None, format=None):
        data = request.data
        todo_to_update = Flyer.objects.get(pk=pk)
        if request.FILES.get("file", None) is not None:
            img = request.FILES["file"]
            img_name = str(uuid.uuid4()) + "-" + os.path.splitext(img.name)[0]
            img_extension = os.path.splitext(img.name)[1]
            save_path = "media/flyers" # + folder #+ "/"
            if not os.path.exists(save_path):
                os.makedirs(save_path)
            
            img_save_path = "%s/%s%s" % (save_path, img_name, img_extension)
            with open(img_save_path, "wb+") as f:
                for chunk in img.chunks():
                    f.write(chunk)
                    
            serializer = flyerSerializer(instance=todo_to_update,data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save(image=img_name + img_extension)
            # response = Response()
            feedback = {
                'message': 'Flyer Updated Successfully',
                'data': serializer.data,
            }
        # ...
        else:
            serializer = flyerSerializer(instance=todo_to_update,data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            # response = Response()
            feedback = {
                'message': 'Flyer Updated Successfully',
                'data': serializer.data,
            }
        return Response(feedback)
        # serializer = flyerSerializer(instance=todo_to_update,data=request.data, partial=True)
        # serializer.is_valid(raise_exception=True)
        # serializer.save()
        # response = Response()
        # response.data = {
        #     'message': 'Flyer Updated Successfully',
        #     'data': serializer.data
        # }
        # return response

    def delete(self, request, pk, format=None):
        todo_to_delete =  Flyer.objects.get(pk=pk)

        todo_to_delete.delete()

        return Response({
            'message': 'Flyer Deleted Successfully'
        })