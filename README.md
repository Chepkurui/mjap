# mjap

Sample APIs for mjap

To run this application:
- Please ensure that you have python and pip installed
- Create and activate a virutual environment
- Run pip install -r requirements.txt in the project root directory
- Open postman and test the APIs
You may also use {server}:8000/swagger
